package com.joaodjunior.jdjone.app.android.actions;

import com.joaodjunior.jdjone.app.functions.Platform;
import com.joaodjunior.jdjone.app.functions.WifiService;

public class AndroidPlatform extends Platform {

	private AndroidWifiService service;
	@Override
	public WifiService getWifiService() {
		if(service == null) {
			service = new AndroidWifiService(service.getActivity());
		}
		return service;
	}

}
