package com.joaodjunior.jdjone.app.android.actions;

import com.joaodjunior.jdjone.app.android.actions.WifiListener.InternetCheckListener;
import com.joaodjunior.jdjone.app.functions.WifiService;

import javafxports.android.FXActivity;

public class AndroidWifiService implements WifiService {
	
	private FXActivity activity;
	
	private boolean isConnected;

	public AndroidWifiService(FXActivity activity) {
		super();
		this.activity = activity;
	}

	@Override
	public boolean verifyWifi() {
		getListener().isInternetConnectionAvailable(new InternetCheckListener() {
			@Override public void onComplete(boolean connected) {
				setConnected(isConnected);
			}
		});
		return isConnected();
	}

	@Override
	public void verifyMobileData() {
		// TODO Auto-generated method stub
		
	}
	
	public FXActivity getActivity() {
		return activity;
	}

	public void setActivity(FXActivity activity) {
		this.activity = activity;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}



	private final WifiListener listener = new WifiListener(getActivity());



	public WifiListener getListener() {
		return listener;
	}
	
	

}
