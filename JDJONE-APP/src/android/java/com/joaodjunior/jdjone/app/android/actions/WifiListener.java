package com.joaodjunior.jdjone.app.android.actions;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.joaodjunior.jdjone.app.functions.PlatformFactory;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import javafxports.android.FXActivity;

public class WifiListener extends AsyncTask<Void, Void, Void> {
	
	// esse codigo esta em: https://stackoverflow.com/questions/6493517/detect-if-android-device-has-internet-connection/6493572#42018966
	private FXActivity activity;
	private InternetCheckListener listener;
	//final private static String CONNECTIVITY_SERVICE = "";
	
	public WifiListener(FXActivity activity) {
		this.activity = activity;
	}

	@Override
	protected Void doInBackground(Void... parametros) {
		
		
		boolean b = hasInternetAccess();
        listener.onComplete(b);

        return null;
    }


    public void isInternetConnectionAvailable(InternetCheckListener x){
        listener=x;
        execute();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
    private boolean hasInternetAccess() {
        if (isNetworkAvailable()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("https://google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Android");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 204 &&
                        urlc.getContentLength() == 0);
            } catch (IOException e) {
            	Logger.getLogger(PlatformFactory.class.getName()).log(Level.SEVERE, null, e);
            }
        } else {
        	Logger.getLogger(PlatformFactory.class.getName()).log(Level.SEVERE, null, "Internet n�o ativa");
        }
        return false;
    }
    
    public interface InternetCheckListener {
        void onComplete(boolean connected);
    }

}
