package com.joaodjunior.jdjone.app;

import java.util.Locale;

import com.gluonhq.charm.glisten.afterburner.AppView;
import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.afterburner.AppViewRegistry;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.joaodjunior.jdjone.app.model.LoginAuth;
import com.joaodjunior.jdjone.app.report.model.Empresa;
import com.joaodjunior.jdjone.app.views.AcessoPresenter;
import com.joaodjunior.jdjone.app.views.AnaliticoPresenter;
import com.joaodjunior.jdjone.app.views.BuscaRelatorioPresenter;
import com.joaodjunior.jdjone.app.views.BuscarPresenter;
import com.joaodjunior.jdjone.app.views.BuscarView;
import com.joaodjunior.jdjone.app.views.EmpresaPresenter;
import com.joaodjunior.jdjone.app.views.FaturamentoPresenter;
import com.joaodjunior.jdjone.app.views.HomePresenter;
import com.joaodjunior.jdjone.app.views.RedesPresenter;

import javafx.scene.layout.AnchorPane;

import static com.gluonhq.charm.glisten.afterburner.AppView.Flag.*;

public class AppViewManager {
	
	private static LoginAuth login;
	private static Empresa empresa;
	
	public static final AppViewRegistry REGISTRY = new AppViewRegistry();
	
	public static final AppView ACESSO_VIEW = view("Acesso", AcessoPresenter.class, MaterialDesignIcon.BEACH_ACCESS);
	public static final AppView HOME_VIEWS = view("JDJONE - Home", HomePresenter.class, MaterialDesignIcon.HOME, SHOW_IN_DRAWER);
	public static final AppView BUSCA_VIEW = view("JDJONE - Busca", BuscarPresenter.class, MaterialDesignIcon.DASHBOARD, SHOW_IN_DRAWER);
	public static final AppView ANALITICO_VIEW = view("JDJONE - Busca", AnaliticoPresenter.class, MaterialDesignIcon.DASHBOARD, SHOW_IN_DRAWER);
	public static final AppView FATURAMENTO_VIEW = view("JDJONE - Busca", FaturamentoPresenter.class, MaterialDesignIcon.DASHBOARD, SHOW_IN_DRAWER);
	public static final AppView REDE_VIEW = view("JDJONE - Busca", RedesPresenter.class, MaterialDesignIcon.DASHBOARD, SHOW_IN_DRAWER);
	public static final AppView RELATORIO_VIEW = view("JDJONE - Busca", BuscaRelatorioPresenter.class, MaterialDesignIcon.DASHBOARD, SHOW_IN_DRAWER);
	public static final AppView EMPRESA_VIEW = view("JDJONE - Empresa", EmpresaPresenter.class, MaterialDesignIcon.ACCOUNT_BOX);
	
	private static AppView view(String title, Class<? extends GluonPresenter<?>> presenterClass, MaterialDesignIcon menuIcon, AppView.Flag... flags) {
		return REGISTRY.createView(name(presenterClass), title, presenterClass, menuIcon, flags);
	}
	
	private static String name(Class<? extends GluonPresenter<?>> presenterClass) {
        return presenterClass.getSimpleName().toUpperCase(Locale.ROOT).replace("PRESENTER", "");
    }
    
    public static void registerViews(MobileApplication app) {
        for (AppView view : REGISTRY.getViews()) {
            view.registerView(app);
        }
    }

	public static LoginAuth getLogin() {
		return login;
	}

	public static void setLogin(LoginAuth login) {
		AppViewManager.login = login;
	}

	public static Empresa getEmpresa() {
		return empresa;
	}

	public static void setEmpresa(Empresa empresa) {
		AppViewManager.empresa = empresa;
	}
	
	public static void avisoView() {
		
	}
	
	public static AnchorPane buscaView() {
		//FXMLLoader loader = new FXMLLoader(AppViewManager.class.getResource("login.fxml"));
		View view = new BuscarView().getView();
		return new AnchorPane(view);
	}
    
}
