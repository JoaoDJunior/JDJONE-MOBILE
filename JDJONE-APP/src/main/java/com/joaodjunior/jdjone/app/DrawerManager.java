package com.joaodjunior.jdjone.app;

import com.gluonhq.charm.down.Platform;
import com.gluonhq.charm.down.Services;
import com.gluonhq.charm.down.plugins.LifecycleService;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.NavigationDrawer;
import com.gluonhq.charm.glisten.control.NavigationDrawer.Item;
import com.gluonhq.charm.glisten.control.NavigationDrawer.ViewItem;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import static com.joaodjunior.jdjone.app.JdjOne.*;
import javafx.scene.image.Image;

public class DrawerManager {

    public static void buildDrawer(MobileApplication app) {
        NavigationDrawer drawer = app.getDrawer();
        
        NavigationDrawer.Header header = new NavigationDrawer.Header("JDJ ONE",
                "Pluton version",
                new Avatar(21, new Image(DrawerManager.class.getResourceAsStream("/icon.png"))));
        drawer.setHeader(header);
        
        final Item homeItem = new ViewItem("Home", MaterialDesignIcon.HOME.graphic(), HOME_VIEWS);
        final Item empresaItem = new ViewItem("Empresa", MaterialDesignIcon.DASHBOARD.graphic(), EMPRESA_VIEW);
        final Item buscaItem = new ViewItem("Buscar", MaterialDesignIcon.DASHBOARD.graphic(), BUSCA_VIEW);
        final Item analiticoItem = new ViewItem("Analiticos", MaterialDesignIcon.DASHBOARD.graphic(), ANALITICO_VIEW);
        final Item faturamentosItem = new ViewItem("Faturamentos", MaterialDesignIcon.DASHBOARD.graphic(), FATURAMENTO_VIEW);
        final Item redesItem = new ViewItem("Redes", MaterialDesignIcon.DASHBOARD.graphic(), REDE_VIEW);
        drawer.getItems().addAll(homeItem, empresaItem, buscaItem, analiticoItem, faturamentosItem, redesItem);
        
        if (Platform.isDesktop()) {
            final Item quitItem = new Item("Quit", MaterialDesignIcon.EXIT_TO_APP.graphic());
            quitItem.selectedProperty().addListener((obs, ov, nv) -> {
                if (nv) {
                    Services.get(LifecycleService.class).ifPresent(LifecycleService::shutdown);
                }
            });
            drawer.getItems().add(quitItem);
        }
    }
}