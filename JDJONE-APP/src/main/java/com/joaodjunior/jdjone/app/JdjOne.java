package com.joaodjunior.jdjone.app;

import com.joaodjunior.jdjone.app.views.LoginView;
import com.joaodjunior.jdjone.app.views.RedesView;
import com.joaodjunior.jdjone.app.views.AnaliticoView;
import com.joaodjunior.jdjone.app.views.BuscaRelatorioView;
import com.joaodjunior.jdjone.app.views.EmpresaView;
import com.joaodjunior.jdjone.app.views.FaturamentoView;
import com.joaodjunior.jdjone.app.views.HomeView;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class JdjOne extends MobileApplication {

    public static final String LOGIN_VIEW = HOME_VIEW;
    public static final String HOME_VIEWS = "Home View";
    public static final String BUSCA_VIEW = "Busca View";
    public static final String EMPRESA_VIEW = "Empresa View";
    public static final String ANALITICO_VIEW = "Analitico View";
    public static final String REDE_VIEW = "Rede View";
    public static final String FATURAMENTO_VIEW = "Faturamento View";
    
    @Override
    public void init() {
    	
        addViewFactory(LOGIN_VIEW, () -> new LoginView().getView());
        addViewFactory(HOME_VIEWS, () -> new HomeView().getView());
        addViewFactory(BUSCA_VIEW, () -> new BuscaRelatorioView().getView());
        addViewFactory(EMPRESA_VIEW, () -> new EmpresaView().getView());
        addViewFactory(ANALITICO_VIEW, () -> new AnaliticoView().getView());
        addViewFactory(REDE_VIEW, () -> new RedesView().getView());
        addViewFactory(FATURAMENTO_VIEW, () -> new FaturamentoView().getView());
        AppViewManager.registerViews(this);
        DrawerManager.buildDrawer(this);
    }

    @Override
    public void postInit(Scene scene) {
        Swatch.BLUE.assignTo(scene);

        scene.getStylesheets().add(JdjOne.class.getResource("style.css").toExternalForm());
        ((Stage) scene.getWindow()).getIcons().add(new Image(JdjOne.class.getResourceAsStream("/icon.png")));
    }
}
