package com.joaodjunior.jdjone.app.dao;

import java.io.IOException;
import java.util.List;

import com.joaodjunior.jdjone.app.model.Analitico;
import com.joaodjunior.jdjone.app.report.model.Empresa;
import com.joaodjunior.jdjone.app.repository.AnaliticoRepository;
import com.joaodjunior.jdjone.app.repository.EmpresaRepository;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AnaliticoDao {
	
	private AnaliticoRepository repository;
	
	final private static String HEROKU = "https://jdjone-ws.herokuapp.com";
	
	public List<Analitico> buscarAnaliticos(Long empresaId) throws IOException {
		Retrofit retrofit = new Retrofit.Builder().baseUrl(HEROKU).addConverterFactory(GsonConverterFactory.create()).build();
		
		this.repository = retrofit.create(AnaliticoRepository.class);
		
		Call<List<Analitico>> empresas = this.repository.getAllAnalitico(empresaId);
		
		return empresas.execute().body();
	}

}
