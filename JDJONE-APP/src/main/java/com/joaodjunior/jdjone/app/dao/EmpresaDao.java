package com.joaodjunior.jdjone.app.dao;

import java.io.IOException;
import java.util.List;

import com.joaodjunior.jdjone.app.report.model.Empresa;
import com.joaodjunior.jdjone.app.repository.EmpresaRepository;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmpresaDao {

	
	private EmpresaRepository repository;
	
	final private static String HEROKU = "https://jdjone-ws.herokuapp.com";
	
	public List<Empresa> buscarEmpresas() throws IOException {
		Retrofit retrofit = new Retrofit.Builder().baseUrl(HEROKU).addConverterFactory(GsonConverterFactory.create()).build();
		
		this.repository = retrofit.create(EmpresaRepository.class);
		
		Call<List<Empresa>> empresas = this.repository.getAllEmpresa();
		
		return empresas.execute().body();
	}
	
	public Empresa buscarEmpresa(Long id) throws IOException {
		Retrofit retrofit = new Retrofit.Builder().baseUrl(HEROKU).addConverterFactory(GsonConverterFactory.create()).build();
		
		this.repository = retrofit.create(EmpresaRepository.class);
		
		Call<Empresa> empresa = this.repository.getEmpresaById(id);
		
		return empresa.execute().body();
	}
	
}
