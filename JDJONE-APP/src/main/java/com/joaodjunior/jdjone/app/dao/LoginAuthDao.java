package com.joaodjunior.jdjone.app.dao;

import java.io.IOException;

import com.joaodjunior.jdjone.app.model.LoginAuth;
import com.joaodjunior.jdjone.app.repository.LoginAuthRepository;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginAuthDao {
	
	
	private LoginAuthRepository repository;
	
	final private static String HEROKU = "https://jdjone-ws.herokuapp.com";
	
	public LoginAuth makeLogin(String email, String senha) throws IOException {
		Retrofit retrofit = new Retrofit.Builder().baseUrl(HEROKU).addConverterFactory(GsonConverterFactory.create()).build();
		
		this.repository = retrofit.create(LoginAuthRepository.class);
		
		Call<LoginAuth> login = this.repository.loginWithtEmailSenha(email, senha);
		
		return login.execute().body();
	}

	public LoginAuth makeAcess(String email, String senha) {
		return null;
	}

}
