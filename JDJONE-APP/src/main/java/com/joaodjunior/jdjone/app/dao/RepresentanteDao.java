package com.joaodjunior.jdjone.app.dao;

import java.io.IOException;

import com.joaodjunior.jdjone.app.report.model.Representante;
import com.joaodjunior.jdjone.app.repository.RepresentanteRepository;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RepresentanteDao {
	
	private RepresentanteRepository repository;
	
	final private static String HEROKU = "https://jdjone-ws.herokuapp.com";
	
	public Representante buscarRepresentante(Long id) throws IOException {
		Retrofit retrofit = new Retrofit.Builder().baseUrl(HEROKU).addConverterFactory(GsonConverterFactory.create()).build();
		
		this.repository = retrofit.create(RepresentanteRepository.class);
		
		Call<Representante> representante = this.repository.getRepresentanteById(id);
		
		return representante.execute().body();
	}
	
	public Representante buscarRepresentante(String nome) throws IOException {
		Retrofit retrofit = new Retrofit.Builder().baseUrl(HEROKU).addConverterFactory(GsonConverterFactory.create()).build();
		
		this.repository = retrofit.create(RepresentanteRepository.class);
		
		Call<Representante> representante = this.repository.getRepresentanteByNome(nome);
		
		return representante.execute().body();
	}

}
