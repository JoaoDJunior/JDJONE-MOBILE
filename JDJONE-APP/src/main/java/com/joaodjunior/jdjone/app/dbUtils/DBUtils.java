package com.joaodjunior.jdjone.app.dbUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.gluonhq.charm.down.Platform;
import com.gluonhq.charm.down.Services;
import com.gluonhq.charm.down.plugins.StorageService;

public class DBUtils {

	private final static String DB_NAME = "sample.db";

	private Connection connection = null;
	private Statement stmt;
	private ResultSet rs;

	public static void copyDatabase(String pathIni, String pathEnd, String name) {

		try (InputStream myInput = DBUtils.class.getResourceAsStream(pathIni + name)) {
			String outFileName = pathEnd + "/" + name;
			try (OutputStream myOutput = new FileOutputStream(outFileName)) {
				byte[] buffer = new byte[1024];
				int length;
				while ((length = myInput.read(buffer)) > 0) {
					myOutput.write(buffer, 0, length);
				}
				myOutput.flush();

			} catch (IOException ex) {
				System.out.println("Error " + ex);
			}
		} catch (IOException ex) {
			System.out.println("Error " + ex);
		}
	}

	private void createDB() {

		File dir;
		String dbUrl = "jdbc:sqlite:";
		try {
			dir = Services.get(StorageService.class).map(s -> s.getPrivateStorage().get())
					.orElseThrow(() -> new IOException("Error: PrivateStorage not available"));
			File db = new File(dir, DB_NAME);
			dbUrl = dbUrl + db.getAbsolutePath();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		try {
			connection = DriverManager.getConnection(dbUrl);
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		try {
			if (connection != null) {
				stmt = connection.createStatement();
				stmt.setQueryTimeout(30);
				
				stmt.executeUpdate("drop table if exists person");
				for(String query : QueryFactory.createTableQueries() ) {
					stmt.executeUpdate(query);
				}
				stmt.executeUpdate("insert into person values(1, 'Johan', 'Vos')");
				stmt.executeUpdate("insert into person values(2, 'Eugene', 'Ryzhikov')");
				stmt.executeUpdate("insert into person values(3, 'Joeri', 'Sykora')");
				stmt.executeUpdate("insert into person values(4, 'Erwin', 'Morrhey')");

				rs = stmt.executeQuery("select * from person");
			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException ex) {
				throw new RuntimeException(ex);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException ex) {
				throw new RuntimeException(ex);
			}
		}

	}

	private void readDB() {

		String dbUrl = "jdbc:sqlite:";
		if (Platform.isDesktop()) {
			dbUrl = dbUrl + ":resource:" + DBUtils.class.getResource("/databases/" + DB_NAME).toExternalForm();
		} else {

			File dir;
			try {
				dir = Services.get(StorageService.class).map(s -> s.getPrivateStorage().get())
						.orElseThrow(() -> new IOException("Error: PrivateStorage not available"));
				File db = new File(dir, DB_NAME);
				DBUtils.copyDatabase("/databases/", dir.getAbsolutePath(), DB_NAME);
				dbUrl = dbUrl + db.getAbsolutePath();
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
		}

		try {
			connection = DriverManager.getConnection(dbUrl);
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		try {
			DatabaseMetaData md = connection.getMetaData();
			rs = md.getTables(null, null, "%", null);
			while (rs.next()) {

			}

			stmt = connection.createStatement();
			rs = stmt.executeQuery("select * from person");
			while (rs.next()) {
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");

			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException ex) {
				throw new RuntimeException(ex);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException ex) {
				throw new RuntimeException(ex);
			}
		}

	}
}
