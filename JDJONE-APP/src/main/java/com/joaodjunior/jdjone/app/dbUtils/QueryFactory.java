package com.joaodjunior.jdjone.app.dbUtils;

import java.util.ArrayList;
import java.util.List;

public class QueryFactory {
	
	public static List<String> createTableQueries() {
		
		List<String> queries = new ArrayList<String>();
		
		queries.add(" create table analitico (id integer, mes integer, ano integer, id_empresa integer, id_faturamento integer, id_faturamentoMensal integer, id_aprovadas integer, id_recuperacao integer, id_statusLoja integer, id_fpd integer, id_crliq integer);");
		queries.add(" create table aprovadas (id integer, qtdEmitidasAcumulada integer, qtdAprovadas integer, qtdCompra integer); ");
		queries.add(" create table crliq (id integer, saldo double, extrato double, percentual double); ");
		queries.add(" create table empresa (id integer, cnpj string, razaoSocial string, nomeFantasia string, rede string, cidade string, uf string, id_representante integer); ");
		queries.add(" create table faturamento (id integer, faturamentoPl double, faturamentoHib double, faturamentoHibOffUs double, faturamentoHibOnUs double, faturamentoOnUs double, faturamentoDia double, faturamentoTotal double ); ");
		queries.add(" create table faturamentoTotalMensal (id integer, segmentoLojista string, faturamentoLoja double); ");
		queries.add(" create table fpd (id integer, saldo double, extrato double, percentual double); ");
		queries.add(" create table login (matricula integer, username string, email string, senha string, token string, tipo string, representante_id integer); ");
		queries.add(" create table recuperacao (id integer, valor double); ");
		queries.add(" create table representante (id integer, grc string, ga string, regionalTricard string, acessorAtendimento string, regionalTribanco string, gerenteComercialTribanco string, cidade string, uf string); ");
		queries.add(" create table seguroFatura (id integer, aprovadas integer, seguros integer, faturasEmail integer); ");
		queries.add(" create table statusLoja (id integer, lojaPositiva string, lojaAprovando string, lojaAtiva string); ");
		
		return queries;
	}

}
