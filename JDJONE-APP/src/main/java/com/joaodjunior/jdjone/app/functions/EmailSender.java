package com.joaodjunior.jdjone.app.functions;
/*
import java.io.StringWriter;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.joaodjunior.jdjone.app.model.ParametroSistema;
*/
public class EmailSender {
	
	/*private ParametroSistema parametros;

	private String user;
	private String passwd;
	private String smtp;
	private String porta;
	private boolean habilitarSsl;
	private String envioEmail;

	private Session session;

	public EmailSender(ParametroSistema parametros) throws Exception {
		this.parametros = parametros;
		if (this.parametros == null) {
			throw new Exception("Sem parametros de configuração de e-mail");
		} else {
			user = this.parametros.getUsuarioEmail();
			passwd = this.parametros.getSenhaEmail();
			smtp = this.parametros.getServidorSmtp();
			envioEmail = this.parametros.getEmailEnvio();
			porta = this.parametros.getPortaSmtp();
			habilitarSsl = (this.parametros.getHabilitarSsl() == null) ? true
					: (this.parametros.getHabilitarSsl().equals(SimNaoEnum.SIM.getValor()));
		}
	}
	
	private String preparaMensagem() {
		VelocityEngine ve = new VelocityEngine();

		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());

		ve.init();

		VelocityContext velocity = new VelocityContext();
		velocity.put("adv", advogado);
		velocity.put("diasLimite", diasLimite);
		velocity.put("lista", paraEnvio);
		velocity.put("hoje", DataUtils.pegaDataFormatada(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		velocity.put("periodicidade", periodicidade);

		StringWriter writer = new StringWriter();

		if (coordenador)
			ve.mergeTemplate(diretorioArquivoVM.concat(arquivoVMCoordenador), "UTF-8", velocity, writer);
		else {
			ve.mergeTemplate(diretorioArquivoVM.concat(arquivoVMAdvogado), "UTF-8", velocity, writer);
		}

		return writer.toString();
	}

	public void reconectSession() {
		Properties props = new Properties();
		props.put("mail.smtp.host", this.smtp);
		props.put("mail.smtp.socketFactory.port", this.porta);

		if (this.habilitarSsl)
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", this.porta);
		this.session = Session.getDefaultInstance(props, new Autenticador(user, passwd));
	}

	public void SendEmail(String assunto, String mensagem, String[] destinatarios) throws MessagingException {

		if (destinatarios.length == 0) return;
		
		if (this.session == null)
			reconectSession();

		MimeMessage message = new MimeMessage(this.session);
		message.setFrom(new InternetAddress(this.envioEmail)); // Remetente

		String dest = "";
		for (int i = 0; i < destinatarios.length; i++) {
			dest += destinatarios[i];
			if (i != destinatarios.length - 1)
				dest += ",";
		}

		message.setRecipients(Message.RecipientType.TO, dest);
		message.setSubject(assunto);// Assunto
		message.setText(mensagem, "utf-8", "html");

		Transport.send(message);// Envio de fato da menssagem

	}

	public ParametroSistema getParametros() {
		return parametros;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getSmtp() {
		return smtp;
	}

	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}

	public String getPorta() {
		return porta;
	}

	public void setPorta(String porta) {
		this.porta = porta;
	}

	public boolean isHabilitarSsl() {
		return habilitarSsl;
	}

	public void setHabilitarSsl(boolean habilitarSsl) {
		this.habilitarSsl = habilitarSsl;
	}

	public void setParametros(ParametroSistema parametros) {
		this.parametros = parametros;
	}*/

}
/*
class Autenticador extends javax.mail.Authenticator {

	private String user;
	private String passwd;

	public Autenticador(String user, String passwd) {
		this.user = user;
		this.passwd = passwd;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(this.user, this.passwd);
	}

}*/
