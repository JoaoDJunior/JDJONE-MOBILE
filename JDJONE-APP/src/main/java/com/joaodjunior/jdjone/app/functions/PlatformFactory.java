package com.joaodjunior.jdjone.app.functions;

import java.util.logging.Level;
import java.util.logging.Logger;

public class PlatformFactory {
	
	 public static Platform getPlatform() {
		 try {
			 return (Platform) Class.forName(getPlatformClassName()).newInstance();
		 } catch (Throwable ex) {
			 Logger.getLogger(PlatformFactory.class.getName()).log(Level.SEVERE, null, ex);
	         return null;
	      }

	    }

	    private static String getPlatformClassName() {
	        switch ( System.getProperty("javafx.platform", "desktop") ) {
	            case "android": return "org.jpereda.sms.AndroidPlatform";
	            case "ios": return "org.jpereda.sms.IosPlatform";
	            default : return "org.jpereda.sms.DesktopPlatform";
	        }
	    }

}
