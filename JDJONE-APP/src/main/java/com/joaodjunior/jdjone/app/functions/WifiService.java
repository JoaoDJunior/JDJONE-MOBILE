package com.joaodjunior.jdjone.app.functions;

public interface WifiService {
	
	boolean verifyWifi();
	
	void verifyMobileData();

}
