package com.joaodjunior.jdjone.app.model;


public class Analitico {
	
	private Long id;
	private int mes;
	private int ano;
	
	private Empresa empresa;
	
	private Faturamento faturamento;
	
	private FaturamentoTotalMensal faturamentoTotalMensal;
	
	private Aprovadas aprovadas;
	
	private RecuperacaoLoja recuperacao;
	
	private StatusLoja statusLoja;
	
	private FPD fpd;
	
	private CRLiQ creLiq;
	
	public Analitico() {
		super();
	}
	
	public Analitico(Long id, int mes, int ano) {
		super();
		this.id = id;
		this.mes = mes;
		this.ano = ano;
	}

	public Analitico(Long id, int mes, int ano, Empresa empresa) {
		super();
		this.id = id;
		this.mes = mes;
		this.ano = ano;
		this.empresa = empresa;
	}

	public Analitico(int mes, int ano, Empresa empresa, Faturamento faturamento,
			FaturamentoTotalMensal faturamentoTotalMensal, Aprovadas aprovadas, RecuperacaoLoja recuperacao,
			StatusLoja statusLoja, FPD fpd, CRLiQ creLiq) {
		super();
		this.mes = mes;
		this.ano = ano;
		this.empresa = empresa;
		this.faturamento = faturamento;
		this.faturamentoTotalMensal = faturamentoTotalMensal;
		this.aprovadas = aprovadas;
		this.recuperacao = recuperacao;
		this.statusLoja = statusLoja;
		this.fpd = fpd;
		this.creLiq = creLiq;
	}

	public Analitico(int mes, int ano) {
		this.mes = mes;
		this.ano = ano;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Faturamento getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Faturamento faturamento) {
		this.faturamento = faturamento;
	}

	public Aprovadas getAprovadas() {
		return aprovadas;
	}

	public void setAprovadas(Aprovadas aprovadas) {
		this.aprovadas = aprovadas;
	}

	public RecuperacaoLoja getRecuperacao() {
		return recuperacao;
	}

	public void setRecuperacao(RecuperacaoLoja recuperacao) {
		this.recuperacao = recuperacao;
	}

	public FaturamentoTotalMensal getFaturamentoTotalMensal() {
		return faturamentoTotalMensal;
	}

	public void setFaturamentoTotalMensal(FaturamentoTotalMensal faturamentoTotalMensal) {
		this.faturamentoTotalMensal = faturamentoTotalMensal;
	}

	public StatusLoja getStatusLoja() {
		return statusLoja;
	}

	public void setStatusLoja(StatusLoja statusLoja) {
		this.statusLoja = statusLoja;
	}

	public FPD getFpd() {
		return fpd;
	}

	public void setFpd(FPD fpd) {
		this.fpd = fpd;
	}

	public CRLiQ getCreLiq() {
		return creLiq;
	}

	public void setCreLiq(CRLiQ creLiq) {
		this.creLiq = creLiq;
	}


	
	
}
