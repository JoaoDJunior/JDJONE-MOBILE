package com.joaodjunior.jdjone.app.model;

public class Cartao {

	private String id;
	
	private String produto;
	
	//VERIFICAR A RELAÇÂO
	//@ManyToMany
	private Empresa empresa;

	public Cartao() {
		super();
	}

	public Cartao(String id, String produto, Empresa empresa) {
		super();
		this.id = id;
		this.produto = produto;
		this.empresa = empresa;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	
	
}
