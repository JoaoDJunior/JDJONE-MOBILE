package com.joaodjunior.jdjone.app.model;


public class Contas {
	
	
	private Long id;
	private int contasAptasPl;
	private int contasUsoPl;
	private int contasInativasPl;
	private int contasAptasHib;
	private int contasUsoHib;
	private int contasInativasHib;
	
	public Contas() {
		super();
	}

	public Contas(Long id, int contasAptasPl, int contasUsoPl, int contasInativasPl, int contasAptasHib,
			int contasUsoHib, int contasInativasHib) {
		super();
		this.id = id;
		this.contasAptasPl = contasAptasPl;
		this.contasUsoPl = contasUsoPl;
		this.contasInativasPl = contasInativasPl;
		this.contasAptasHib = contasAptasHib;
		this.contasUsoHib = contasUsoHib;
		this.contasInativasHib = contasInativasHib;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getContasAptasPl() {
		return contasAptasPl;
	}

	public void setContasAptasPl(int contasAptasPl) {
		this.contasAptasPl = contasAptasPl;
	}

	public int getContasUsoPl() {
		return contasUsoPl;
	}

	public void setContasUsoPl(int contasUsoPl) {
		this.contasUsoPl = contasUsoPl;
	}

	public int getContasInativasPl() {
		return contasInativasPl;
	}

	public void setContasInativasPl(int contasInativasPl) {
		this.contasInativasPl = contasInativasPl;
	}

	public int getContasAptasHib() {
		return contasAptasHib;
	}

	public void setContasAptasHib(int contasAptasHib) {
		this.contasAptasHib = contasAptasHib;
	}

	public int getContasUsoHib() {
		return contasUsoHib;
	}

	public void setContasUsoHib(int contasUsoHib) {
		this.contasUsoHib = contasUsoHib;
	}

	public int getContasInativasHib() {
		return contasInativasHib;
	}

	public void setContasInativasHib(int contasInativasHib) {
		this.contasInativasHib = contasInativasHib;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + contasAptasHib;
		result = prime * result + contasAptasPl;
		result = prime * result + contasInativasHib;
		result = prime * result + contasInativasPl;
		result = prime * result + contasUsoHib;
		result = prime * result + contasUsoPl;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contas other = (Contas) obj;
		if (contasAptasHib != other.contasAptasHib)
			return false;
		if (contasAptasPl != other.contasAptasPl)
			return false;
		if (contasInativasHib != other.contasInativasHib)
			return false;
		if (contasInativasPl != other.contasInativasPl)
			return false;
		if (contasUsoHib != other.contasUsoHib)
			return false;
		if (contasUsoPl != other.contasUsoPl)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Contas [id=");
		builder.append(id);
		builder.append(", contasAptasPl=");
		builder.append(contasAptasPl);
		builder.append(", contasUsoPl=");
		builder.append(contasUsoPl);
		builder.append(", contasInativasPl=");
		builder.append(contasInativasPl);
		builder.append(", contasAptasHib=");
		builder.append(contasAptasHib);
		builder.append(", contasUsoHib=");
		builder.append(contasUsoHib);
		builder.append(", contasInativasHib=");
		builder.append(contasInativasHib);
		builder.append("]");
		return builder.toString();
	}
	
}
