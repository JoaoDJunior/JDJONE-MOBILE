package com.joaodjunior.jdjone.app.model;


public class FPD {

	private Long id;
	private double saldo;
	private double extrato;
	private double percentual;
	
	public FPD() {
		super();
	}

	public FPD(Long id, double saldo, double extrato, double percentual) {
		super();
		this.id = id;
		this.saldo = saldo;
		this.extrato = extrato;
		this.percentual = percentual;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getExtrato() {
		return extrato;
	}

	public void setExtrato(double extrato) {
		this.extrato = extrato;
	}

	public double getPercentual() {
		return percentual;
	}

	public void setPercentual(double percentual) {
		this.percentual = percentual;
	}
	
	
}
