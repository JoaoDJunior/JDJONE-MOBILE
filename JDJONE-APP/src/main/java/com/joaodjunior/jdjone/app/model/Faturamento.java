package com.joaodjunior.jdjone.app.model;

public class Faturamento   {

	
	private Long id;
	private double faturamentoPl;	//Dentro da loja sempre
	private double faturamentoHib;	//Soma do faturamento HIB fora e dentro da loja
	private double faturamentoHibOffUs;
	private double faturamentoHibOnUs;
	private double faturamentoOnUs;	// Soma de faturamentoHibOnUS e faturamentoPl
	private double faturamentoDia;
	private double faturamentoTotal;
	
	public Faturamento() {
		super();
	}
	
	public Faturamento(double faturamentoPl, double faturamentoHib, double faturamentoHibOffUs,
			double faturamentoHibOnUs, double faturamentoOnUs, double faturamentoDia, double faturamentoTotal) {
		super();
		this.faturamentoPl = faturamentoPl;
		this.faturamentoHib = faturamentoHib;
		this.faturamentoHibOffUs = faturamentoHibOffUs;
		this.faturamentoHibOnUs = faturamentoHibOnUs;
		this.faturamentoOnUs = faturamentoOnUs;
		this.faturamentoDia = faturamentoDia;
		this.faturamentoTotal = faturamentoTotal;
	}

	public Faturamento(Long id, double faturamentoPl, double faturamentoHib, double faturamentoHibOffUs,
			double faturamentoHibOnUs, double faturamentoOnUs, double faturamentoDia, double faturamentoTotal) {
		super();
		this.id = id;
		this.faturamentoPl = faturamentoPl;
		this.faturamentoHib = faturamentoHib;
		this.faturamentoHibOffUs = faturamentoHibOffUs;
		this.faturamentoHibOnUs = faturamentoHibOnUs;
		this.faturamentoOnUs = faturamentoOnUs;
		this.faturamentoDia = faturamentoDia;
		this.faturamentoTotal = faturamentoTotal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getFaturamentoPl() {
		return faturamentoPl;
	}

	public void setFaturamentoPl(double faturamentoPl) {
		this.faturamentoPl = faturamentoPl;
	}

	public double getFaturamentoHib() {
		return faturamentoHib;
	}

	public void setFaturamentoHib(double faturamentoHib) {
		this.faturamentoHib = faturamentoHib;
	}

	public double getFaturamentoHibOffUs() {
		return faturamentoHibOffUs;
	}

	public void setFaturamentoHibOffUs(double faturamentoHibOffUs) {
		this.faturamentoHibOffUs = faturamentoHibOffUs;
	}

	public double getFaturamentoHibOnUs() {
		return faturamentoHibOnUs;
	}

	public void setFaturamentoHibOnUs(double faturamentoHibOnUs) {
		this.faturamentoHibOnUs = faturamentoHibOnUs;
	}

	public double getFaturamentoOnUs() {
		return faturamentoOnUs;
	}

	public void setFaturamentoOnUs(double faturamentoOnUs) {
		this.faturamentoOnUs = faturamentoOnUs;
	}

	public double getFaturamentoDia() {
		return faturamentoDia;
	}

	public void setFaturamentoDia(double faturamentoDia) {
		this.faturamentoDia = faturamentoDia;
	}

	public double getFaturamentoTotal() {
		return faturamentoTotal;
	}

	public void setFaturamentoTotal(double faturamentoTotal) {
		this.faturamentoTotal = faturamentoTotal;
	}

}
