package com.joaodjunior.jdjone.app.model;


public class FaturamentoTotalMensal {
	
	private Long id;
	private String segmentoLojista;
	private Double faturamentoLoja;
	
	public FaturamentoTotalMensal() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FaturamentoTotalMensal(String segmentoLojista, Double faturamentoLoja) {
		super();
		this.segmentoLojista = segmentoLojista;
		this.faturamentoLoja = faturamentoLoja;
	}

	public FaturamentoTotalMensal(Long id, String segmentoLojista, Double faturamentoLoja) {
		super();
		this.id = id;
		this.segmentoLojista = segmentoLojista;
		this.faturamentoLoja = faturamentoLoja;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSegmentoLojista() {
		return segmentoLojista;
	}

	public void setSegmentoLojista(String segmentoLojista) {
		this.segmentoLojista = segmentoLojista;
	}

	public Double getFaturamentoLoja() {
		return faturamentoLoja;
	}

	public void setFaturamentoLoja(Double faturamentoLoja) {
		this.faturamentoLoja = faturamentoLoja;
	}
	
	

}
