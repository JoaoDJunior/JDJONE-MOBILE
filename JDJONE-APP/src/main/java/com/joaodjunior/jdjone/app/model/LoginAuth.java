package com.joaodjunior.jdjone.app.model;


public class LoginAuth {
	

	private Long id;
	private String username;
	private String email;
	private String senha;
	
	private String token;
	
	private String tipo;
	
	private Representante representante;

	public LoginAuth() {
		super();
	}

	public LoginAuth(Long id, String username, String email, String senha, String token, String tipo, Representante representante) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.senha = senha;
		this.token = token;
		this.tipo = tipo;
		this.representante = representante;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Representante getRepresentante() {
		return representante;
	}

	public void setRepresentante(Representante representante) {
		this.representante = representante;
	}

}
