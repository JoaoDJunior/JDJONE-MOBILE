package com.joaodjunior.jdjone.app.model;

public class ParametroSistema {
	
	private Long codigo;

	private String servidorSmtp;	

	private String usuarioEmail;

	private String senhaEmail;

	private String portaSmtp;

	private String habilitarSsl;

	private String emailEnvio;
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getServidorSmtp() {
		return servidorSmtp;
	}

	public void setServidorSmtp(String servidorSmtp) {
		this.servidorSmtp = servidorSmtp;
	}

	public String getUsuarioEmail() {
		return usuarioEmail;
	}

	public void setUsuarioEmail(String usuarioEmail) {
		this.usuarioEmail = usuarioEmail;
	}

	public String getSenhaEmail() {
		return senhaEmail;
	}

	public void setSenhaEmail(String senhaEmail) {
		this.senhaEmail = senhaEmail;
	}

	public String getPortaSmtp() {
		return portaSmtp;
	}

	public void setPortaSmtp(String portaSmtp) {
		this.portaSmtp = portaSmtp;
	}

	public String getHabilitarSsl() {
		return habilitarSsl;
	}

	public void setHabilitarSsl(String habilitarSsl) {
		this.habilitarSsl = habilitarSsl;
	}
	
	public String getEmailEnvio() {
		return emailEnvio;
	}

}
