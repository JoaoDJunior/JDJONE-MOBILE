package com.joaodjunior.jdjone.app.model;

import java.util.List;


public class Representante {

	private Long id;
	private String grc;
	private String ga;
	private String regionalTricard;
	private String acessorAtendimento;
	private String regionalTribanco;
	private String gerenteComercialTribanco;
	private String cidade;
	private String uf;
	
	private List<Empresa> empresas;

	public Representante() {
		super();
	}

	public Representante(Long id, String grc, String ga, String regionalTricard, String acessorAtendimento,
			String regionalTribanco, String gerenteComercialTribanco, String cidade, String uf) {
		super();
		this.id = id;
		this.grc = grc;
		this.ga = ga;
		this.regionalTricard = regionalTricard;
		this.acessorAtendimento = acessorAtendimento;
		this.regionalTribanco = regionalTribanco;
		this.gerenteComercialTribanco = gerenteComercialTribanco;
		this.cidade = cidade;
		this.uf = uf;
	}



	public Representante(String grc, String ga, String regionalTricard, String acessorAtendimento,
			String regionalTribanco, String gerenteComercialTribanco, String cidade, String uf) {
		this.grc = grc;
		this.ga = ga;
		this.regionalTricard = regionalTricard;
		this.acessorAtendimento = acessorAtendimento;
		this.regionalTribanco = regionalTribanco;
		this.gerenteComercialTribanco = gerenteComercialTribanco;
		this.cidade = cidade;
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGrc() {
		return grc;
	}

	public void setGrc(String grc) {
		this.grc = grc;
	}

	public String getGa() {
		return ga;
	}

	public void setGa(String ga) {
		this.ga = ga;
	}

	public String getRegional() {
		return regionalTricard;
	}

	public void setRegional(String regionalTricard) {
		this.regionalTricard = regionalTricard;
	}

	public String getRegionalTricard() {
		return regionalTricard;
	}

	public void setRegionalTricard(String regionalTricard) {
		this.regionalTricard = regionalTricard;
	}

	public String getAcessorAtendimento() {
		return acessorAtendimento;
	}

	public void setAcessorAtendimento(String acessorAtendimento) {
		this.acessorAtendimento = acessorAtendimento;
	}

	public String getRegionalTribanco() {
		return regionalTribanco;
	}

	public void setRegionalTribanco(String regionalTribanco) {
		this.regionalTribanco = regionalTribanco;
	}

	public String getGerenteComercialTribanco() {
		return gerenteComercialTribanco;
	}

	public void setGerenteComercialTribanco(String gerenteComercialTribanco) {
		this.gerenteComercialTribanco = gerenteComercialTribanco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}
	
	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acessorAtendimento == null) ? 0 : acessorAtendimento.hashCode());
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((empresas == null) ? 0 : empresas.hashCode());
		result = prime * result + ((ga == null) ? 0 : ga.hashCode());
		result = prime * result + ((gerenteComercialTribanco == null) ? 0 : gerenteComercialTribanco.hashCode());
		result = prime * result + ((grc == null) ? 0 : grc.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((regionalTribanco == null) ? 0 : regionalTribanco.hashCode());
		result = prime * result + ((regionalTricard == null) ? 0 : regionalTricard.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Representante other = (Representante) obj;
		if (acessorAtendimento == null) {
			if (other.acessorAtendimento != null)
				return false;
		} else if (!acessorAtendimento.equals(other.acessorAtendimento))
			return false;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (empresas == null) {
			if (other.empresas != null)
				return false;
		} else if (!empresas.equals(other.empresas))
			return false;
		if (ga == null) {
			if (other.ga != null)
				return false;
		} else if (!ga.equals(other.ga))
			return false;
		if (gerenteComercialTribanco == null) {
			if (other.gerenteComercialTribanco != null)
				return false;
		} else if (!gerenteComercialTribanco.equals(other.gerenteComercialTribanco))
			return false;
		if (grc == null) {
			if (other.grc != null)
				return false;
		} else if (!grc.equals(other.grc))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (regionalTribanco == null) {
			if (other.regionalTribanco != null)
				return false;
		} else if (!regionalTribanco.equals(other.regionalTribanco))
			return false;
		if (regionalTricard == null) {
			if (other.regionalTricard != null)
				return false;
		} else if (!regionalTricard.equals(other.regionalTricard))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Representante [id=");
		builder.append(id);
		builder.append(", grc=");
		builder.append(grc);
		builder.append(", ga=");
		builder.append(ga);
		builder.append(", regionalTricard=");
		builder.append(regionalTricard);
		builder.append(", acessorAtendimento=");
		builder.append(acessorAtendimento);
		builder.append(", regionalTribanco=");
		builder.append(regionalTribanco);
		builder.append(", gerenteComercialTribanco=");
		builder.append(gerenteComercialTribanco);
		builder.append(", cidade=");
		builder.append(cidade);
		builder.append(", uf=");
		builder.append(uf);
		builder.append(", empresas=");
		builder.append(empresas);
		builder.append("]");
		return builder.toString();
	}
	
	

}
