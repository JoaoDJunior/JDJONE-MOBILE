package com.joaodjunior.jdjone.app.model;


public class SeguroFatura {

	private Long id;
	private int aprovadas;
	private int seguro;
	private int faturasEmail;
	
	public SeguroFatura() {
		super();
	}

	public SeguroFatura(Long id, int aprovadas, int seguro, int faturasEmail) {
		super();
		this.id = id;
		this.aprovadas = aprovadas;
		this.seguro = seguro;
		this.faturasEmail = faturasEmail;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getAprovadas() {
		return aprovadas;
	}

	public void setAprovadas(int aprovadas) {
		this.aprovadas = aprovadas;
	}

	public int getSeguro() {
		return seguro;
	}

	public void setSeguro(int seguro) {
		this.seguro = seguro;
	}

	public int getFaturasEmail() {
		return faturasEmail;
	}

	public void setFaturasEmail(int faturasEmail) {
		this.faturasEmail = faturasEmail;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + aprovadas;
		result = prime * result + faturasEmail;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + seguro;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeguroFatura other = (SeguroFatura) obj;
		if (aprovadas != other.aprovadas)
			return false;
		if (faturasEmail != other.faturasEmail)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (seguro != other.seguro)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SeguroFatura [id=");
		builder.append(id);
		builder.append(", aprovadas=");
		builder.append(aprovadas);
		builder.append(", seguro=");
		builder.append(seguro);
		builder.append(", faturasEmail=");
		builder.append(faturasEmail);
		builder.append("]");
		return builder.toString();
	}

}
