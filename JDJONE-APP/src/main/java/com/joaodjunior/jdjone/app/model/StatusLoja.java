package com.joaodjunior.jdjone.app.model;

public class StatusLoja {
	
	private Long id;
	private String lojaPositiva;
	private String lojaAprovando;
	private String lojaAtiva;
	
	public StatusLoja() {
		super();
	}
	
	public StatusLoja(String lojaPositiva, String lojaAprovando, String lojaAtiva) {
		super();
		this.lojaPositiva = lojaPositiva;
		this.lojaAprovando = lojaAprovando;
		this.lojaAtiva = lojaAtiva;
	}

	public StatusLoja(Long id, String lojaPositiva, String lojaAprovando, String lojaAtiva) {
		super();
		this.id = id;
		this.lojaPositiva = lojaPositiva;
		this.lojaAprovando = lojaAprovando;
		this.lojaAtiva = lojaAtiva;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLojaPositiva() {
		return lojaPositiva;
	}

	public void setLojaPositiva(String lojaPositiva) {
		this.lojaPositiva = lojaPositiva;
	}

	public String getLojaAprovando() {
		return lojaAprovando;
	}

	public void setLojaAprovando(String lojaAprovando) {
		this.lojaAprovando = lojaAprovando;
	}

	public String getLojaAtiva() {
		return lojaAtiva;
	}

	public void setLojaAtiva(String lojaAtiva) {
		this.lojaAtiva = lojaAtiva;
	}
	
	

}
