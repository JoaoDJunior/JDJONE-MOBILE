package com.joaodjunior.jdjone.app.report.model;

import java.util.List;

public class Apresentacao {
	
	private Empresa empresa;
	private String periodo;
	private Representante representante;
	private List<Faturamento> faturamentos;
	private List<Aprovadas> aprovadas;
	private List<Contas> contas;
	private Inadimplencia inadimplencia;
	private List<RecuperacaoLoja> recuperacao;
	private List<Seguro> seguro;
	private List<CartaoPJ> cartao;
	private List<StatusLoja> statusLoja;

	public Apresentacao() {
		super();
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Representante getRepresentante() {
		return representante;
	}

	public void setRepresentante(Representante representante) {
		this.representante = representante;
	}

	public List<Faturamento> getFaturamentos() {
		return faturamentos;
	}

	public void setFaturamentos(List<Faturamento> faturamentos) {
		this.faturamentos = faturamentos;
	}

	public List<Aprovadas> getAprovadas() {
		return aprovadas;
	}

	public void setAprovadas(List<Aprovadas> aprovadas) {
		this.aprovadas = aprovadas;
	}

	public List<Contas> getContas() {
		return contas;
	}

	public void setContas(List<Contas> contas) {
		this.contas = contas;
	}

	public Inadimplencia getInadimplencia() {
		return inadimplencia;
	}

	public void setInadimplencia(Inadimplencia inadimplencia) {
		this.inadimplencia = inadimplencia;
	}

	public List<RecuperacaoLoja> getRecuperacao() {
		return recuperacao;
	}

	public void setRecuperacao(List<RecuperacaoLoja> recuperacao) {
		this.recuperacao = recuperacao;
	}

	public List<Seguro> getSeguro() {
		return seguro;
	}

	public void setSeguro(List<Seguro> seguro) {
		this.seguro = seguro;
	}

	public List<CartaoPJ> getCartao() {
		return cartao;
	}

	public void setCartao(List<CartaoPJ> cartao) {
		this.cartao = cartao;
	}

	public List<StatusLoja> getStatusLoja() {
		return statusLoja;
	}

	public void setStatusLoja(List<StatusLoja> statusLoja) {
		this.statusLoja = statusLoja;
	}
	
	
	
	
}
