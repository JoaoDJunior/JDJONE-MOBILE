package com.joaodjunior.jdjone.app.report.model;

public class Aprovadas {
	
	private Empresa empresa;
	private int emitidas;
	private int aprovadas;
	private int comCompra;
	private int propostasPendentes;
	private Double percEmitidas;
	private Double aprovDeltaAno;
	private Double compraDeltaAno;
	
	public Aprovadas() {
		super();
	}

	public Aprovadas(Empresa empresa, int emitidas, int aprovadas, int comCompra,
			int propostasPendentes, Double percEmitidas, Double aprovDeltaAno,
			Double compraDeltaAno) {
		super();
		this.empresa = empresa;
		this.emitidas = emitidas;
		this.aprovadas = aprovadas;
		this.comCompra = comCompra;
		this.propostasPendentes = propostasPendentes;
		this.percEmitidas = percEmitidas;
		this.aprovDeltaAno = aprovDeltaAno;
		this.compraDeltaAno = compraDeltaAno;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public int getEmitidas() {
		return emitidas;
	}

	public void setEmitidas(int emitidas) {
		this.emitidas = emitidas;
	}

	public int getAprovadas() {
		return aprovadas;
	}

	public void setAprovadas(int aprovadas) {
		this.aprovadas = aprovadas;
	}

	public int getComCompra() {
		return comCompra;
	}

	public void setComCompra(int comCompra) {
		this.comCompra = comCompra;
	}

	public int getPropostasPendentes() {
		return propostasPendentes;
	}

	public void setPropostasPendentes(int propostasPendentes) {
		this.propostasPendentes = propostasPendentes;
	}

	public Double getPercEmitidas() {
		return percEmitidas;
	}

	public void setPercEmitidas(Double percEmitidas) {
		this.percEmitidas = percEmitidas;
	}

	public Double getAprovDeltaAno() {
		return aprovDeltaAno;
	}

	public void setAprovDeltaAno(Double aprovDeltaAno) {
		this.aprovDeltaAno = aprovDeltaAno;
	}

	public Double getCompraDeltaAno() {
		return compraDeltaAno;
	}

	public void setCompraDeltaAno(Double compraDeltaAno) {
		this.compraDeltaAno = compraDeltaAno;
	}

}
