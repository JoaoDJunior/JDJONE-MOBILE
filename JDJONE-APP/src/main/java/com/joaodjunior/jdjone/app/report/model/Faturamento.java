package com.joaodjunior.jdjone.app.report.model;

public class Faturamento {
	
	private Long id;
	private Double pl;
	private Double hib;
	private Double fatPl;
	private Double fatHib;
	private Double delta;
	
	public Faturamento() {
		super();
	}

	public Faturamento(Long id, Double pl, Double hib, Double fatPl, Double fatHib, Double delta) {
		super();
		this.id = id;
		this.pl = pl;
		this.hib = hib;
		this.fatPl = fatPl;
		this.fatHib = fatHib;
		this.delta = delta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPl() {
		return pl;
	}

	public void setPl(Double pl) {
		this.pl = pl;
	}

	public Double getHib() {
		return hib;
	}

	public void setHib(Double hib) {
		this.hib = hib;
	}

	public Double getFatPl() {
		return fatPl;
	}

	public void setFatPl(Double fatPl) {
		this.fatPl = fatPl;
	}

	public Double getFatHib() {
		return fatHib;
	}

	public void setFatHib(Double fatHib) {
		this.fatHib = fatHib;
	}

	public Double getDelta() {
		return delta;
	}

	public void setDelta(Double delta) {
		this.delta = delta;
	}
	
	
	
	

}
