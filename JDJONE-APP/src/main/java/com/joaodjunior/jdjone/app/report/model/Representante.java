package com.joaodjunior.jdjone.app.report.model;

import java.util.List;

public class Representante {

	private Long id;
	private String grc;
	private String ga;
	private String regional;
	
	private List<Empresa> empresas;

	public Representante() {
		super();
	}

	public Representante(Long id, String grc, String ga, String regional, List<Empresa> empresas) {
		super();
		this.id = id;
		this.grc = grc;
		this.ga = ga;
		this.regional = regional;
		this.empresas = empresas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGrc() {
		return grc;
	}

	public void setGrc(String grc) {
		this.grc = grc;
	}

	public String getGa() {
		return ga;
	}

	public void setGa(String ga) {
		this.ga = ga;
	}

	public String getRegional() {
		return regional;
	}

	public void setRegional(String regional) {
		this.regional = regional;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}
	
	
}
