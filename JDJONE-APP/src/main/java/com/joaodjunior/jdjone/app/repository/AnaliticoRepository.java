package com.joaodjunior.jdjone.app.repository;

import java.util.List;

import com.joaodjunior.jdjone.app.model.Analitico;
import com.joaodjunior.jdjone.app.report.model.Empresa;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface AnaliticoRepository {
	
	@GET("/api/empresa/{empresaId}/analitico")
	Call<List<Analitico>> getAllAnalitico(@Path("empresaId") Long empresaId);

}
