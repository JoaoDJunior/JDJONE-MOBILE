package com.joaodjunior.jdjone.app.repository;

import java.util.List;

import com.joaodjunior.jdjone.app.report.model.Empresa;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface EmpresaRepository {
	
	@GET("/api/empresa/")
	Call<List<Empresa>> getAllEmpresa();
	
	@GET("/api/empresa/{id}")
	Call<Empresa> getEmpresaById(@Path("id") Long id);
	
	//@GET("/api/empresa")

}
