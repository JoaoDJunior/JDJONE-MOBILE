package com.joaodjunior.jdjone.app.repository;

import com.joaodjunior.jdjone.app.model.LoginAuth;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface LoginAuthRepository {

	@GET("/api/login/{email}/{senha}")
	Call<LoginAuth> loginWithtEmailSenha(@Path("email") String email, @Path("senha") String senha);
	
}
