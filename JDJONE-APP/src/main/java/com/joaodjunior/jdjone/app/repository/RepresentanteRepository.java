package com.joaodjunior.jdjone.app.repository;

import com.joaodjunior.jdjone.app.report.model.Representante;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RepresentanteRepository {
	
	@GET("/api/representante/{id}")
	Call<Representante> getRepresentanteById(@Path("id") Long id);
	
	@GET("/api/representante/{nome}")
	Call<Representante> getRepresentanteByNome(@Path("nome") String nome);

}
