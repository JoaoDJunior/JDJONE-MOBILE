package com.joaodjunior.jdjone.app.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.joaodjunior.jdjone.app.JdjOne;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class AcessoPresenter extends GluonPresenter<JdjOne> {
	
	@FXML
    private View home;

    @FXML
    private JFXTextField tfMatricula;

    @FXML
    private JFXTextField tfUsername;

    @FXML
    private JFXTextField tfEmail;

    @FXML
    private JFXPasswordField pfSenha;

    @FXML
    private JFXButton btnEnviar;
    
    @FXML
    void initialize() {

    }

    @FXML
    void SolicitarAcesso(ActionEvent event) {

    }

}
