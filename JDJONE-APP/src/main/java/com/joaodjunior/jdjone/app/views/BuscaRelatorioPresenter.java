package com.joaodjunior.jdjone.app.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.CharmListView;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.joaodjunior.jdjone.app.AppViewManager;
import com.joaodjunior.jdjone.app.JdjOne;
import com.joaodjunior.jdjone.app.report.model.Empresa;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class BuscaRelatorioPresenter extends GluonPresenter<JdjOne> {
	
	@FXML
    private View home;

    @FXML
    private JFXComboBox<String> cbMes;

    @FXML
    private JFXComboBox<String> cbAno;

    @FXML
    private JFXButton btnBuscar;

    @FXML
    private CharmListView<Empresa, String> lvRelatorios;

    public void initialize() {
		home.setShowTransitionFactory(BounceInRightTransition::new);
		FloatingActionButton fab = new FloatingActionButton(MaterialDesignIcon.INFO.text,
                e -> System.out.println("Info"));
        fab.showOn(home);
        home.showingProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> 
                        MobileApplication.getInstance().getDrawer().open()));
                appBar.setTitleText("Buscar relatorios");
                appBar.getActionItems().add(MaterialDesignIcon.FAVORITE.button(e -> 
                        System.out.println("Favorite")));
            }
        });
        
		
	}
    
    @FXML
    void buscaPorPeriodo(ActionEvent event) {

    }

}
