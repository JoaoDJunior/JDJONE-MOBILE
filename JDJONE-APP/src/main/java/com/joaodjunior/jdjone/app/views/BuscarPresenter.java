package com.joaodjunior.jdjone.app.views;

import java.net.URL;
import java.util.ResourceBundle;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.control.CharmListView;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.joaodjunior.jdjone.app.JdjOne;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;

public class BuscarPresenter extends GluonPresenter<JdjOne> {
	
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private View home;

    @FXML
    private JFXComboBox<?> cbFuncao;

    @FXML
    private HBox hbox;

    @FXML
    private JFXComboBox<Integer> cbMes;

    @FXML
    private JFXComboBox<Integer> cbAno;

    @FXML
    private JFXButton btnBuscar;

    @FXML
    private CharmListView<?, ?> lvBusca;

    private ObservableList<Integer> mesList = FXCollections.observableArrayList();
    private ObservableList<Integer> anoList = FXCollections.observableArrayList();
    private ObservableList<Integer> FuncaoList = FXCollections.observableArrayList();
    

    @FXML
    void initialize() {
        assert home != null : "fx:id=\"home\" was not injected: check your FXML file 'buscar.fxml'.";
        assert cbFuncao != null : "fx:id=\"cbMes1\" was not injected: check your FXML file 'buscar.fxml'.";
        assert hbox != null : "fx:id=\"hbox\" was not injected: check your FXML file 'buscar.fxml'.";
        assert cbMes != null : "fx:id=\"cbMes\" was not injected: check your FXML file 'buscar.fxml'.";
        assert cbAno != null : "fx:id=\"cbAno\" was not injected: check your FXML file 'buscar.fxml'.";
        assert btnBuscar != null : "fx:id=\"btnBuscar\" was not injected: check your FXML file 'buscar.fxml'.";
        assert lvBusca != null : "fx:id=\"lvBusca\" was not injected: check your FXML file 'buscar.fxml'.";
    	this.mesList.addAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);    	
		this.anoList.addAll(2016, 217, 2018);
    	this.cbMes.setItems(mesList);
    	this.cbAno.setItems(anoList);
    }
    @FXML
    private void buscar(ActionEvent event) {
    	
    }

}
