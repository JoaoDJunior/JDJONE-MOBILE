package com.joaodjunior.jdjone.app.views;

import java.io.IOException;

import com.gluonhq.charm.glisten.mvc.View;

import javafx.fxml.FXMLLoader;

public class BuscarView {
	
	public View getView() {
        try {
            View view = FXMLLoader.load(BuscarView.class.getResource("buscar.fxml"));
            return view;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
