package com.joaodjunior.jdjone.app.views;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.CharmListCell;
import com.gluonhq.charm.glisten.control.CharmListView;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.control.Icon;
import com.gluonhq.charm.glisten.control.ListTile;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.joaodjunior.jdjone.app.AppViewManager;
import com.joaodjunior.jdjone.app.JdjOne;
import com.joaodjunior.jdjone.app.dao.AnaliticoDao;
import com.joaodjunior.jdjone.app.dao.RepresentanteDao;
import com.joaodjunior.jdjone.app.functions.Relatorio;
import com.joaodjunior.jdjone.app.model.Analitico;
import com.joaodjunior.jdjone.app.model.Apresentacao;
import com.joaodjunior.jdjone.app.model.Empresa;
import com.joaodjunior.jdjone.app.model.Faturamento;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class EmpresaPresenter extends GluonPresenter<JdjOne> {
	
	@FXML
    private View home;

    @FXML
    private AnchorPane pane;

    @FXML
    private CharmListView<Empresa, String> lvEmpresas;
    
    private static ObservableList<Empresa> empresasList = FXCollections.observableArrayList();
	
	private Empresa empresa;
	
	public void initialize() throws IOException {
		home.setShowTransitionFactory(BounceInRightTransition::new);
		FloatingActionButton fab = new FloatingActionButton(MaterialDesignIcon.INFO.text,
                e -> System.out.println("Info"));
        fab.showOn(home);
        for(Empresa empresa : AppViewManager.getLogin().getRepresentante().getEmpresas()) {
        	System.out.println(empresa.toString());
        	empresasList.add(empresa);
        }
        home.showingProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> 
                        MobileApplication.getInstance().getDrawer().open()));
                if(empresa != null) { 
                	appBar.setTitleText(empresa.getNomeFantasia());
                }
                appBar.getActionItems().add(MaterialDesignIcon.FAVORITE.button(e -> 
                        System.out.println("Favorite")));
            }
        });
        
        //this.buscarRelatorios();
        this.setLista();
        
        Platform.runLater(() -> {
        	for (Node node : lvEmpresas.getChildrenUnmodifiable()) {
        		if(node instanceof ListView) {
        			((ListView)node).getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
        				System.out.println(nv);
        				//Empresa e = (Empresa) nv;
        				//this.viewEmpresa(e); Ver o detalhamento do dado
        			});
        			
        		}
        	}
        });
        
        
		
	}
	private void setLista() throws IOException {
		
		if(empresasList != null) {
			
			this.lvEmpresas.setItems(empresasList);
			lvEmpresas.setCellFactory(p -> new CharmListCell<Empresa>() {
				public void updateItem(Empresa empresa, boolean empty) {
					super.updateItem(empresa, empty);
					if(empresa != null && !empty) {
						ListTile tile = new ListTile();
						tile.textProperty().addAll(empresa.getRazaoSocial() + " - " +empresa.getCidade()+"-"+empresa.getUf(),
								"CNPJ: "+empresa.getCnpj()+", Nome fantasia: "+ empresa.getNomeFantasia() + ", Rede: "+ empresa.getRede());
						final Image image = new Image(HomePresenter.class.getResourceAsStream("/logo.png"));
						if(image!=null){
							tile.setPrimaryGraphic(new ImageView(image));
						}
						tile.setSecondaryGraphic(new Icon(MaterialDesignIcon.KEYBOARD_ARROW_RIGHT));
						setText(null);
						setGraphic(tile);
					} else {
						setText(null);
						setGraphic(null);
					}
				}
			});
			lvEmpresas.setHeadersFunction(Empresa::getRede);
			
			lvEmpresas.setHeaderCellFactory(p -> new CharmListCell<Empresa>() {
				public void updateItem(Empresa empresa, boolean empty) {
					super.updateItem(empresa, empty); 
					if(empresa != null && !empty){
						ListTile tile = new ListTile();
						tile.textProperty().addAll("rede",lvEmpresas.toString(empresa));
						Avatar avatar = new Avatar(20);
						avatar.setImage(new Image(HomePresenter.class.getResourceAsStream("/logo.png")));
						tile.setPrimaryGraphic(avatar);
						setText(null);
						setGraphic(tile);
					} else {
						setText(null);
						setGraphic(null);
					}
				}
			});
		}
 
    }
	
	/*private void buscarRelatorios() throws IOException {
		empresasList = FXCollections.observableArrayList();
    	List<Empresa> empresas= new AnaliticoDao().buscarAnaliticos(empresa.getId());
    	for(Empresa a : empresas) {
    		relatoriosList.add(a);
    	}
    	this.lvEmpresas.setItems(empresasList);
    	this.lvEmpresas.setCellFactory(p -> new CharmListCell<Empresa>() {
    		public void updateItem(Empresa empresa, boolean empty) {
    			super.updateItem(empresa, empty);
    			if(empresa != null && !empty) {
    				ListTile tile = new ListTile();
    				tile.textProperty().addAll();
    				Button acao = new Button();
    				acao.setGraphic(new Icon(MaterialDesignIcon.FILE_DOWNLOAD));
    				acao.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent arg0) {
							try {
								System.out.println("Clicked: " + analitico.toString());
								gerarRelatorio(analitico);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
    				tile.setSecondaryGraphic(acao);
    				setText(null);
    	            setGraphic(tile);
    	        } else {
    	            setText(null);
    	            setGraphic(null);
    	        }
    		}
    	});
	}*/
	
	/*private void gerarRelatorio(Analitico analitico) {
		List<Apresentacao> apresentacoes = new ArrayList<Apresentacao>();
		Apresentacao ap = new Apresentacao();
		ap.setEmpresa(this.empresa);
		List<Faturamento> fat = new ArrayList<Faturamento>();
		fat.add(analitico.getFaturamento());
		ap.setFaturamentos(fat);
		ap.setFaturamentos(fat);
		ap.setPeriodo(""+analitico.getMes() + analitico.getAno()+"");
		apresentacoes.add(ap);
		try {
			new Relatorio().imprimir(apresentacoes, ap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

}
