package com.joaodjunior.jdjone.app.views;

import java.io.IOException;

import com.gluonhq.charm.glisten.mvc.View;

import javafx.fxml.FXMLLoader;

public class FaturamentoView {
	
	public View getView() {
        try {
            View view = FXMLLoader.load(AcessoView.class.getResource("faturamento.fxml"));
            return view;
        } catch (IOException e) {
            System.out.println("IOException: " + e);
            return new View();
        }
    }

}
