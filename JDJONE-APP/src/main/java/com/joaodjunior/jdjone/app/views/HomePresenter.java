package com.joaodjunior.jdjone.app.views;

import java.io.IOException;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.CharmListCell;
import com.gluonhq.charm.glisten.control.CharmListView;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.control.Icon;
import com.gluonhq.charm.glisten.control.ListTile;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.jfoenix.controls.JFXButton;
import com.joaodjunior.jdjone.app.AppViewManager;
import com.joaodjunior.jdjone.app.JdjOne;
import com.joaodjunior.jdjone.app.dao.RepresentanteDao;
import com.joaodjunior.jdjone.app.model.Empresa;
import com.joaodjunior.jdjone.app.model.Representante;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class HomePresenter extends GluonPresenter<JdjOne> {

	@FXML
    private View home;

    @FXML
    private AnchorPane paneBusca;

    @FXML
    private JFXButton btnEmpresa;

    @FXML
    private JFXButton btnRedes;

    @FXML
    private JFXButton btnFaturamento;

    @FXML
    private JFXButton btnRelatorios;

    @FXML
    private JFXButton btnAnaliticos;

    @FXML
    private JFXButton btnSair;
    
    private Empresa empresa;
    
    private Representante representante;
    
    private static ObservableList<Empresa> relatoriosList;

    public void initialize() throws IOException {
    	
    	representante = AppViewManager.getLogin().getRepresentante();
    	
    	System.out.println(representante.toString());
    	    	
    	this.setBuscaView();
    	
        home.setShowTransitionFactory(BounceInRightTransition::new);
        
        FloatingActionButton fab = new FloatingActionButton(MaterialDesignIcon.INFO.text,
                e -> System.out.println("Info"));
        fab.showOn(home);
        
        home.showingProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> 
                        MobileApplication.getInstance().getDrawer().open()));
                appBar.setTitleText("Home");
            }
        });
        
        /*this.setLista();
        Platform.runLater(() -> {
        	for (Node node : lvEmpresas.getChildrenUnmodifiable()) {
        		if(node instanceof ListView) {
        			((ListView)node).getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
        				System.out.println(nv);
        				Empresa e = (Empresa) nv;
        				this.viewEmpresa(e);
        			});
        			
        		}
        	}
        });*/
    }
    
    private void buscarRelatoriosPorPeriodo() {
    	
    }
    
    private void reload() {
    	
    }
    
    private void setLista() throws IOException {
    	relatoriosList = FXCollections.observableArrayList();
    	
    	for(Empresa e : representante.getEmpresas()) {
    		relatoriosList.add(e);
    	}
    	/*this.lvEmpresas.setItems(relatoriosList);
    	lvEmpresas.setCellFactory(p -> new CharmListCell<Empresa>() {
    		public void updateItem(Empresa empresa, boolean empty) {
    			super.updateItem(empresa, empty);
    			if(empresa != null && !empty) {
    				ListTile tile = new ListTile();
    				tile.textProperty().addAll(empresa.getRazaoSocial() + " - " +empresa.getCidade()+"-"+empresa.getUf(),
    						"CNPJ: "+empresa.getCnpj()+", Nome fantasia: "+ empresa.getNomeFantasia() + ", Rede: "+ empresa.getRede());
    				final Image image = new Image(HomePresenter.class.getResourceAsStream("/logo.png"));
    	            if(image!=null){
    	                tile.setPrimaryGraphic(new ImageView(image));
    	            }
    				tile.setSecondaryGraphic(new Icon(MaterialDesignIcon.KEYBOARD_ARROW_RIGHT));
    				setText(null);
    	            setGraphic(tile);
    	        } else {
    	            setText(null);
    	            setGraphic(null);
    	        }
    		}
    	});
    	lvEmpresas.setHeadersFunction(Empresa::getRede);
    	
    	lvEmpresas.setHeaderCellFactory(p -> new CharmListCell<Empresa>() {
    	    public void updateItem(Empresa empresa, boolean empty) {
    	        super.updateItem(empresa, empty); 
    	        if(empresa != null && !empty){
    	            ListTile tile = new ListTile();
    	            tile.textProperty().addAll("rede",lvEmpresas.toString(empresa));
    	            Avatar avatar = new Avatar(20);
    	            avatar.setImage(new Image(HomePresenter.class.getResourceAsStream("/logo.png")));
    	            tile.setPrimaryGraphic(avatar);
    	            setText(null);
    	            setGraphic(tile);
    	        } else {
    	            setText(null);
    	            setGraphic(null);
    	        }
    	    }
    	});*/
    }
    
    private void getAllRelatorios() {
    	
    }
    
    @FXML
    private void analiticoView(ActionEvent event) {
    	AppViewManager.ANALITICO_VIEW.switchView();
    }
    
    @FXML
    private void empresaView(ActionEvent event) {
    	//AppViewManager.setEmpresa(this.empresa);
    	AppViewManager.EMPRESA_VIEW.switchView();
    }
    
    @FXML
    private void faturamentoView(ActionEvent event) {
    	AppViewManager.FATURAMENTO_VIEW.switchView();
    }

    @FXML
    private void redesView(ActionEvent event) {
    	AppViewManager.REDE_VIEW.switchView();
    }
    
    @FXML
    private void relatorioView(ActionEvent event) {
    	AppViewManager.RELATORIO_VIEW.switchView();
    }

    @FXML
    private void sair(ActionEvent event) {
    	
    }
    
    private void setBuscaView() {
    	this.paneBusca.getChildren().add(AppViewManager.buscaView());
    }
}
