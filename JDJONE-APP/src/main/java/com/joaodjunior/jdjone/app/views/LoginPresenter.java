package com.joaodjunior.jdjone.app.views;

import java.io.IOException;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.joaodjunior.jdjone.app.AppViewManager;
import com.joaodjunior.jdjone.app.JdjOne;
import com.joaodjunior.jdjone.app.dao.LoginAuthDao;
import com.joaodjunior.jdjone.app.model.LoginAuth;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class LoginPresenter extends GluonPresenter<JdjOne>{

    @FXML
    private View login;

    @FXML
    private ImageView ivLogo;

    @FXML
    private Label label;

    @FXML
    private TextField tfEmail;

    @FXML
    private PasswordField tfSenha;

    @FXML
    private Button btnLogin;

    @FXML
    private Button btnAcesso;


    
    private AppBar appBar;

    public void initialize() {
    	
    	/*WifiService service = PlatformFactory.getPlatform().getWifiService();
    	boolean wifi = service.verifyWifi();
    	System.out.println(wifi);
    	if(wifi) {
    		label.setText(String.valueOf(wifi));
    	}*/
    	
    	ivLogo.setImage(new Image(LoginPresenter.class.getResourceAsStream("/logo.png")));
        login.showingProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue) {
                appBar = MobileApplication.getInstance().getAppBar();
                appBar.setTitleText("Login");
            }
        });
    }
    
    @FXML
    private void login(ActionEvent event) throws IOException {
    	//this.appBar.progressBarVisibleProperty().set(true);
    	if(!(tfEmail.getText().isEmpty() && tfSenha.getText().isEmpty())) {
    		LoginAuthDao dao = new LoginAuthDao();
    		LoginAuth auth = dao.makeLogin(tfEmail.getText().toLowerCase(), tfSenha.getText());
    		//new HomePresenter(new RepresentanteDao().buscarRepresentante(auth.getId()));
    		if(auth.getToken() != null) {
    			this.prepararScene(auth);    			
    		} else {
    			this.showToast();
    		}
    		
    	} else {
    		
    	}
    }
    
    private void showToast() {
		
		
	}

	@FXML
    private void solicitarAcesso(ActionEvent event) {
		AppViewManager.ACESSO_VIEW.switchView();
    }
    
    private void prepararScene(LoginAuth auth) {
    	this.appBar.setNavIcon(MaterialDesignIcon.MENU.button(e ->
        	MobileApplication.getInstance().getDrawer().open()));
    	AppViewManager.setLogin(auth);
    	AppViewManager.HOME_VIEWS.switchView();
    }
    
    
    
}
