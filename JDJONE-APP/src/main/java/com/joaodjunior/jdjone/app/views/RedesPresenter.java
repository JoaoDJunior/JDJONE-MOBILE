package com.joaodjunior.jdjone.app.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.CharmListView;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.joaodjunior.jdjone.app.JdjOne;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class RedesPresenter extends GluonPresenter<JdjOne> {

	@FXML
	private View home;

	@FXML
	private JFXTextField tfPesquisa;

	@FXML
	private JFXButton btnPesquisa;

	@FXML
	private CharmListView<?, ?> lvRedes;

	@FXML
	private void initialize() {
home.setShowTransitionFactory(BounceInRightTransition::new);
        
        FloatingActionButton fab = new FloatingActionButton(MaterialDesignIcon.INFO.text,
                e -> System.out.println("Info"));
        fab.showOn(home);
        
        home.showingProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> 
                        MobileApplication.getInstance().getDrawer().open()));
                appBar.setTitleText("Home");
            }
        });
	}

	@FXML
	private void pesquisar(ActionEvent event) {

	}
}
